# #backenmitsinn

Statische Website für soziales Projekt, erreichbar über https://backenmitsinn.at

```
Egal ob Bananenbrot, Karottenkuchen, Zimtschnecken oder Sauerteigbrot 
- alles was du backst, backe einfach doppelt – der Aufwand ist fast der gleiche.
Falls deine Küche nicht die notwendige Kapazität aufbringt,
kannst du auch einfach die Hälfte abgeben.
```

## Mitmachen

Erstelle eine MR oder ein Issue hier in Gitlab oder wende dich an mona@coeln.at